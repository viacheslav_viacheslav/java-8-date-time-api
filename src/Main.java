import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {

        printDayQuantityForEachMonth("2018");
        printAllMondays("2018", Month.OCTOBER.getValue());
        isFridayThirteenth("2018-03-13");
        printTimeElapsed("1986-12-24");
        printCurrentDateInDifferentLocales();

    }

    //1
    public static void printDayQuantityForEachMonth(String year) {
        boolean isLeapYear = Year.parse(year).isLeap();
        Arrays.stream(Month.values()).forEach(month -> System.out.println(month.name() + " " + month.length(isLeapYear)));
    }

    //2
    public static void printAllMondays(String year, int month) {
        YearMonth neededYearMonth = Year.parse(year).atMonth(month);
        IntStream.rangeClosed(1, neededYearMonth.lengthOfMonth())
                .mapToObj(day -> neededYearMonth.atDay(day))
                .filter(day -> day.getDayOfWeek().getValue() == DayOfWeek.MONDAY.getValue())
                .forEach(day -> System.out.println(day+" is "+DayOfWeek.MONDAY));
    }

    //3
    public static void isFridayThirteenth(String date) {
        LocalDate localDate = LocalDate.parse(date);
        if (localDate.getDayOfMonth() == 13 && localDate.getDayOfWeek() == DayOfWeek.FRIDAY) {
            System.out.println(date + " is Friday the 13th");
        } else {
            System.out.println(date + " is not Friday the 13th");
        }
    }

    //4
    public static void printTimeElapsed(String date) {
        LocalDate dateToCompare = LocalDate.parse(date);
        Period elapsedTime = Period.between(dateToCompare, LocalDate.now());
        System.out.println(elapsedTime.getYears() + " year(s) " + elapsedTime.getMonths() + " month(s) " + elapsedTime.getDays() + " day(s)");
    }

    //5
    public static void printCurrentDateInDifferentLocales() {
        Map<String, String> zoneIds = new HashMap<>();
        zoneIds.put("Canada", "Canada/Central");
        zoneIds.put("Germany", "Europe/Berlin");
        zoneIds.put("Pakistan", "Asia/Karachi");
        zoneIds.put("India", "Asia/Calcutta");
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
        zoneIds.forEach((key, value) -> {
            System.out.println(key + ": " + ZonedDateTime.of(LocalDateTime.now(), ZoneId.of(value)).format(formatter));
        });
    }
}
